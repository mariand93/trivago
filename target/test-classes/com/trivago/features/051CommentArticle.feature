Feature: Comment Article

Scenario Outline: Login on trivago and add comment to any article

Given user opens trivago page51
When user clicks on read more
And user clicks on comments
And user clicks on sign is with email
And user enters "<name>" and "<email>" in login form
Then user enters "<comm>" comment
Then user should see that comment is sent and waits for approval

Examples:
	|       name      |    email     |         comm           |
	|    Test Name    | test@email.ro| Automation Test Comment|

Feature: Search Location

Scenario Outline: Search for any location on magazine.trivago.com by using the search bar

Given user opens trivago page1
When user clicks on search icon
And user enters "<city>" in search input
Then user should see search results
And user clicks on first result
Then user should see result opened

Examples:
	| city  |
	| Miami |
		
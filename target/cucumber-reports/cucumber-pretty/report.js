$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("01SearchLocation.feature");
formatter.feature({
  "line": 1,
  "name": "Search Location",
  "description": "",
  "id": "search-location",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Search for any location on magazine.trivago.com by using the search bar",
  "description": "",
  "id": "search-location;search-for-any-location-on-magazine.trivago.com-by-using-the-search-bar",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page1",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on search icon",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user enters \"\u003ccity\u003e\" in search input",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user should see search results",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user clicks on first result",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user should see result opened",
  "keyword": "Then "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "search-location;search-for-any-location-on-magazine.trivago.com-by-using-the-search-bar;",
  "rows": [
    {
      "cells": [
        "city"
      ],
      "line": 13,
      "id": "search-location;search-for-any-location-on-magazine.trivago.com-by-using-the-search-bar;;1"
    },
    {
      "cells": [
        "Miami"
      ],
      "line": 14,
      "id": "search-location;search-for-any-location-on-magazine.trivago.com-by-using-the-search-bar;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "Search for any location on magazine.trivago.com by using the search bar",
  "description": "",
  "id": "search-location;search-for-any-location-on-magazine.trivago.com-by-using-the-search-bar;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page1",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on search icon",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user enters \"Miami\" in search input",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user should see search results",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user clicks on first result",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user should see result opened",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchLocation.user_opens_trivago_page1()"
});
formatter.result({
  "duration": 12184486813,
  "status": "passed"
});
formatter.match({
  "location": "SearchLocation.user_clicks_on_search_icon()"
});
formatter.result({
  "duration": 786000092,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Miami",
      "offset": 13
    }
  ],
  "location": "SearchLocation.user_enters_city_in_search_input(String)"
});
formatter.result({
  "duration": 515396138,
  "status": "passed"
});
formatter.match({
  "location": "SearchLocation.user_should_see_search_results()"
});
formatter.result({
  "duration": 804694720,
  "status": "passed"
});
formatter.match({
  "location": "SearchLocation.user_clicks_on_first_result()"
});
formatter.result({
  "duration": 1044987374,
  "status": "passed"
});
formatter.match({
  "location": "SearchLocation.user_should_see_result_opened()"
});
formatter.result({
  "duration": 3108576073,
  "status": "passed"
});
formatter.uri("02ContactForm.feature");
formatter.feature({
  "line": 1,
  "name": "Contact Form",
  "description": "",
  "id": "contact-form",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Fill in the contact form and send it",
  "description": "",
  "id": "contact-form;fill-in-the-contact-form-and-send-it",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page2",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on footer contact link",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "new tab is opened user should see contact form",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "user enters \"\u003cmessage\u003e\" in Your Message input",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user enters \"\u003cname\u003e\" and \"\u003cemail\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user clicks on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user should see successfully sent message",
  "keyword": "Then "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "contact-form;fill-in-the-contact-form-and-send-it;",
  "rows": [
    {
      "cells": [
        "message",
        "name",
        "email"
      ],
      "line": 14,
      "id": "contact-form;fill-in-the-contact-form-and-send-it;;1"
    },
    {
      "cells": [
        "Hello. This is an Automation Test message. Kind Regards",
        "Test Name",
        "test@email.ro"
      ],
      "line": 15,
      "id": "contact-form;fill-in-the-contact-form-and-send-it;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 15,
  "name": "Fill in the contact form and send it",
  "description": "",
  "id": "contact-form;fill-in-the-contact-form-and-send-it;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page2",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on footer contact link",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "new tab is opened user should see contact form",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "user enters \"Hello. This is an Automation Test message. Kind Regards\" in Your Message input",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user enters \"Test Name\" and \"test@email.ro\"",
  "matchedColumns": [
    1,
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user clicks on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user should see successfully sent message",
  "keyword": "Then "
});
formatter.match({
  "location": "ContactForm.user_opens_trivago_page2()"
});
formatter.result({
  "duration": 9105420569,
  "status": "passed"
});
formatter.match({
  "location": "ContactForm.user_clicks_on_footer_contact_link()"
});
formatter.result({
  "duration": 598352178,
  "status": "passed"
});
formatter.match({
  "location": "ContactForm.new_tab_is_opened_user_should_see_contact_form()"
});
formatter.result({
  "duration": 2271682068,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Hello. This is an Automation Test message. Kind Regards",
      "offset": 13
    }
  ],
  "location": "ContactForm.user_enters_message_in_your_message_input(String)"
});
formatter.result({
  "duration": 2190860798,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test Name",
      "offset": 13
    },
    {
      "val": "test@email.ro",
      "offset": 29
    }
  ],
  "location": "ContactForm.user_enters_name_and_email(String,String)"
});
formatter.result({
  "duration": 1121802686,
  "status": "passed"
});
formatter.match({
  "location": "ContactForm.user_clicks_on_submit_button()"
});
formatter.result({
  "duration": 679929202,
  "status": "passed"
});
formatter.match({
  "location": "ContactForm.user_should_see_successfully_sent_message()"
});
formatter.result({
  "duration": 3188571146,
  "status": "passed"
});
formatter.uri("03NewsletterSubscribe.feature");
formatter.feature({
  "line": 1,
  "name": "Newsletter Subscribe",
  "description": "",
  "id": "newsletter-subscribe",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Subscribe to the Newsletter",
  "description": "",
  "id": "newsletter-subscribe;subscribe-to-the-newsletter",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page3",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enters email in newsletter input",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user clicks on inspire me button",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user should see you are now checked in message",
  "keyword": "Then "
});
formatter.examples({
  "line": 11,
  "name": "",
  "description": "",
  "id": "newsletter-subscribe;subscribe-to-the-newsletter;",
  "rows": [
    {
      "cells": [
        "email"
      ],
      "line": 12,
      "id": "newsletter-subscribe;subscribe-to-the-newsletter;;1"
    },
    {
      "cells": [
        "test@email.com"
      ],
      "line": 13,
      "id": "newsletter-subscribe;subscribe-to-the-newsletter;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 13,
  "name": "Subscribe to the Newsletter",
  "description": "",
  "id": "newsletter-subscribe;subscribe-to-the-newsletter;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page3",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enters email in newsletter input",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user clicks on inspire me button",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user should see you are now checked in message",
  "keyword": "Then "
});
formatter.match({
  "location": "NewsletterSubscribe.user_opens_trivago_page3()"
});
formatter.result({
  "duration": 10720605517,
  "status": "passed"
});
formatter.match({
  "location": "NewsletterSubscribe.user_enters_email_in_newsletter_input()"
});
formatter.result({
  "duration": 1182194829,
  "status": "passed"
});
formatter.match({
  "location": "NewsletterSubscribe.user_clicks_on_inspire_me_button()"
});
formatter.result({
  "duration": 346191401,
  "status": "passed"
});
formatter.match({
  "location": "NewsletterSubscribe.user_should_see_successfully_sent_message()"
});
formatter.result({
  "duration": 4171734178,
  "status": "passed"
});
formatter.uri("04ChooseDestination.feature");
formatter.feature({
  "line": 1,
  "name": "Choose Destination",
  "description": "",
  "id": "choose-destination",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Navigate to a destination through the menu on the top left",
  "description": "",
  "id": "choose-destination;navigate-to-a-destination-through-the-menu-on-the-top-left",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page4",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on hamburger menu icon",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user selects first menu destination",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user should see destination opened",
  "keyword": "Then "
});
formatter.uri("051CommentArticle.feature");
formatter.feature({
  "line": 1,
  "name": "Comment Article",
  "description": "",
  "id": "comment-article",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Login on trivago and add comment to any article",
  "description": "",
  "id": "comment-article;login-on-trivago-and-add-comment-to-any-article",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page51",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on read more",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user clicks on comments",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on sign is with email",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user enters \"\u003cname\u003e\" and \"\u003cemail\u003e\" in login form",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user enters \"\u003ccomm\u003e\" comment",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "user should see that comment is sent and waits for approval",
  "keyword": "Then "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "comment-article;login-on-trivago-and-add-comment-to-any-article;",
  "rows": [
    {
      "cells": [
        "name",
        "email",
        "comm"
      ],
      "line": 14,
      "id": "comment-article;login-on-trivago-and-add-comment-to-any-article;;1"
    },
    {
      "cells": [
        "Test Name",
        "test@email.ro",
        "Automation Test Comment"
      ],
      "line": 15,
      "id": "comment-article;login-on-trivago-and-add-comment-to-any-article;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 15,
  "name": "Login on trivago and add comment to any article",
  "description": "",
  "id": "comment-article;login-on-trivago-and-add-comment-to-any-article;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page51",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on read more",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user clicks on comments",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user clicks on sign is with email",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user enters \"Test Name\" and \"test@email.ro\" in login form",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user enters \"Automation Test Comment\" comment",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "user should see that comment is sent and waits for approval",
  "keyword": "Then "
});
formatter.match({
  "location": "CommentArticle.user_opens_trivago_page51()"
});
formatter.result({
  "duration": 10672388931,
  "status": "passed"
});
formatter.match({
  "location": "CommentArticle.user_clicks_on_read_more()"
});
formatter.result({
  "duration": 587003284,
  "status": "passed"
});
formatter.match({
  "location": "CommentArticle.user_clicks_on_comments()"
});
formatter.result({
  "duration": 3509938388,
  "status": "passed"
});
formatter.match({
  "location": "CommentArticle.user_clicks_on_sign_in_with_email()"
});
formatter.result({
  "duration": 718334525,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test Name",
      "offset": 13
    },
    {
      "val": "test@email.ro",
      "offset": 29
    }
  ],
  "location": "CommentArticle.user_enter_name_and_email_in_login_form(String,String)"
});
formatter.result({
  "duration": 2395269172,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Automation Test Comment",
      "offset": 13
    }
  ],
  "location": "CommentArticle.user_enters_comm_comment(String)"
});
formatter.result({
  "duration": 1123665944,
  "status": "passed"
});
formatter.match({
  "location": "CommentArticle.user_should_see_that_comment_is_sent_and_waits_for_approval()"
});
formatter.result({
  "duration": 1710349666,
  "status": "passed"
});
formatter.uri("052DestinationSearch.feature");
formatter.feature({
  "line": 1,
  "name": "Destination Search",
  "description": "",
  "id": "destination-search",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Search destination and open article",
  "description": "",
  "id": "destination-search;search-destination-and-open-article",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page52",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on search top right icon",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user enters \"\u003ccity\u003e\" in search text area",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user should see destinations results",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user clicks on first article",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user should see article opened",
  "keyword": "Then "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "destination-search;search-destination-and-open-article;",
  "rows": [
    {
      "cells": [
        "city"
      ],
      "line": 13,
      "id": "destination-search;search-destination-and-open-article;;1"
    },
    {
      "cells": [
        "Miami"
      ],
      "line": 14,
      "id": "destination-search;search-destination-and-open-article;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "Search destination and open article",
  "description": "",
  "id": "destination-search;search-destination-and-open-article;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "user opens trivago page52",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on search top right icon",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user enters \"Miami\" in search text area",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user should see destinations results",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user clicks on first article",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user should see article opened",
  "keyword": "Then "
});
formatter.match({
  "location": "DestinationSearch.user_opens_trivago_page52()"
});
formatter.result({
  "duration": 9662269303,
  "status": "passed"
});
formatter.match({
  "location": "DestinationSearch.user_clicks_on_search_top_right_icon()"
});
formatter.result({
  "duration": 767915198,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Miami",
      "offset": 13
    }
  ],
  "location": "DestinationSearch.user_enters_city_in_search_text_area(String)"
});
formatter.result({
  "duration": 657564039,
  "status": "passed"
});
formatter.match({
  "location": "DestinationSearch.user_should_see_destinations_results()"
});
formatter.result({
  "duration": 554327224,
  "status": "passed"
});
formatter.match({
  "location": "DestinationSearch.user_clicks_on_first_article()"
});
formatter.result({
  "duration": 944635482,
  "status": "passed"
});
formatter.match({
  "location": "DestinationSearch.user_should_see_article_opened()"
});
formatter.result({
  "duration": 3407617339,
  "status": "passed"
});
});
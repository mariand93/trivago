Task III – MySQL
Connect to MySQL server with a SQL client using following credentials:
host: pop703qatrial.csps2cqmnya4.eu-west-1.rds.amazonaws.com
username: marsapplicant
password: Mars@trivago2018
Default Schema: marstestdb

Write the queries you would use to answer the following questions:

/* Which department pays the highest salary currently?*/

SELECT Departments.dept_name,
FROM marstestdb.departments Departments
INNER JOIN marstestdb.dept_emp DepartEmployees
        ON Departments.dept_no = DepartEmployees.dept_no
WHERE DepartEmployees.emp_no = (SELECT Salaries.emp_no 
                                FROM marstestdb.salaries Salaries 
                                WHERE Salaries.salary = (SELECT MAX(salary) 
                                                         FROM marstestdb.salaries));
/*-----------------------------------------------------------------------*/

/* How many employees are working in Human Resources Department currently?*/

SELECT COUNT(Employees.emp_no)
FROM marstestdb.employees Employees
INNER JOIN marstestdb.dept_emp DepartEmployees 
        ON Employees.emp_no = DepartEmployees.emp_no
WHERE DepartEmployees.dept_no = (SELECT Depart.dept_no 
                                 FROM marstestdb.departments Depart
                                 WHERE Depart.dept_name = 'Human Resources');
/*-----------------------------------------------------------------------*/

/* Who is currently the oldest employee of the company?*/

SELECT first_name,
       last_name,
       birth_date,
       emp_no
FROM marstestdb.employees 
WHERE birth_date = (SELECT min(birth_date) 
                    FROM marstestdb.employees);
/*-----------------------------------------------------------------------*/

/* What is the current average salary of Marketing Department?*/

SELECT AVG(Salaries.salary)
FROM marstestdb.salaries Salaries
INNER JOIN marstestdb.dept_emp DepartEmployees 
        ON Salaries.emp_no = DepartEmployees.emp_no
WHERE DepartEmployees.dept_no = (SELECT Depart.dept_no 
                                 FROM marstestdb.departments Depart
                                 WHERE Depart.dept_name = 'Marketing');
/*-----------------------------------------------------------------------*/

/* Who is currently the newest employee of the company?*/

SELECT first_name,
       last_name,
       hire_date,
       emp_no
FROM marstestdb.employees 
WHERE hire_date = (SELECT max(hire_date) 
                    FROM marstestdb.employees);
/*-----------------------------------------------------------------------*/
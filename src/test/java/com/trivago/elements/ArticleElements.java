package com.trivago.elements;

public class ArticleElements {
	private static final String articleContentXpath   = "//div[@class='article-content']";
	private static final String destinationTagXpath   = "//a[@class='destination-tag']";
	private static final String searchInputXpath      = "//input[@placeholder='Search']";
	private static final String menuIconXpath         = "//div[@class='nav-icon']";
	private static final String menuXpath             = "//div[@class='header-menu']";
	private static final String footerXpath           = "//span[@id='trivago-magazine-footer']";
	private static final String searchResultsXpath    = "//h3/span[contains(text(), 'Search Results')]";
	private static final String postSectionXpath      = "//section[@class='posts-section']";
	private static final String postCardXpath         = "//div[@class='post-card-image-container']";
	private static final String showCommentsXpath     = "//span[@class='show-comments']";
	private static final String signInEmailXpath      = "//span[contains(text(), 'Sign in with Email')]";
	private static final String loginNameXpath        = "//input[@class='login-input']";
	private static final String loginEmailXpath       = "//input[@type='email']";
	private static final String loginButtonXpath      = "//div[@class='submit-login']";
	private static final String commentInputXpath     = "//textarea[@class='comment-input']";
	private static final String sentCommentXpath      = "//span[@class='comment-btn']";
	private static final String commentSuccessXpath   = "//span[contains(text(), 'Your comment successfully')]";
	
	
	public static String getArticleContentXpath() {
		return articleContentXpath;
	}
	
	public static String getSearchInputXpath() {
		return searchInputXpath;
	}
	
	public static String getDestinationTagXpath() {
		return destinationTagXpath;
	}
	
	public static String getMenuIconXpath() {
		return menuIconXpath;
	}
	
	public static String getMenuXpath() {
		return menuXpath;
	}

	public static String getFooterXpath() {
		return footerXpath;
	}
	
	public static String getSearchResultsXpath() {
		return searchResultsXpath;
	}
	
	public static String getPostCardXpath() {
		return postCardXpath;
	}
	
	public static String getPostSectionXpath() {
		return postSectionXpath;
	}
	
	public static String getShowCommentsXpath() {
		return showCommentsXpath;
	}
	
	public static String getSignInEmailXpath() {
		return signInEmailXpath;
	}
	
	public static String getLoginNameXpath() {
		return loginNameXpath;
	}
	
	public static String getLoginEmailXpath() {
		return loginEmailXpath;
	}
	
	public static String getLoginButtonXpath() {
		return loginButtonXpath;
	}
	
	public static String getCommentInputXpath() {
		return commentInputXpath;
	}
	
	public static String getSentCommentXpath() {
		return sentCommentXpath;
	}
	
	public static String getCommentSuccessXpath() {
		return commentSuccessXpath;
	}
	
}

package com.trivago.elements;

public class SearchElements {
	private static final String searchIconXpath       = "//div[@class='search-icon']";
	private static final String searchXpath           = "//div[@class='search-layer']";
	private static final String searchInputXpath      = "//input[@placeholder='Search']";
	private static final String searchResultsXpath    = "//h3/span[contains(text(), 'Search Results')]";
	private static final String postSectionXpath      = "//section[@class='posts-section']";
	private static final String postCardXpath         = "//div[@class='post-card-image-container']";
	private static final String filterXpath           = "//div[@class='filter-text']";
	private static final String destSectionXpath      = "//div[@class='destination-page-post-section']";
	
	public static String getSearchIconXpath() {
		return searchIconXpath;
	}
	
	public static String getSearchInputXpath() {
		return searchInputXpath;
	}
	
	public static String getSearchXpath() {
		return searchXpath;
	}
	
	public static String getSearchResultsXpath() {
		return searchResultsXpath;
	}
	
	public static String getPostCardXpath() {
		return postCardXpath;
	}
	
	public static String getPostSectionXpath() {
		return postSectionXpath;
	}
	
	public static String getFilterXpath() {
		return filterXpath;
	}
	
	public static String getDestSectionXpath() {
		return destSectionXpath;
	}
}


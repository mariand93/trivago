package com.trivago.elements;

public class ContactElements {
	private static final String headerTextXpath       = "//h2/span[contains(text(), 'Please give us your feedback')]";
	private static final String contactMsgXpath       = "//textarea[@class='contact-msg']";
	private static final String nameInputXpath        = "//input[@class='contact-input']";
	private static final String emailInputXpath       = "//input[@id='contact-email']";
	private static final String submitXpath           = "//button[@class='contact-submit']";
	private static final String confirmXpath          = "//input[@id='confirm']";
	private static final String successMsgXpath       = "//div/p[contains(text(), 'Message Sent Successfully! ')]";
	
	
	public static String getHeaderTextXpath() {
		return headerTextXpath;
	}
	
	public static String getContactMsgXpath() {
		return contactMsgXpath;
	}
	
	public static String getNameInputXpath() {
		return nameInputXpath;
	}
	
	public static String getEmailInputXpath() {
		return emailInputXpath;
	}
	
	public static String getSubmitXpath() {
		return submitXpath;
	}
	
	public static String getConfirmXpath() {
		return confirmXpath;
	}
	
	public static String getSuccessMsgXpath() {
		return successMsgXpath;
	}
}

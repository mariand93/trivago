package com.trivago.elements;

public class HomeElemets {
	private static final String searchIconXpath       = "//div[@class='search-icon']";
	private static final String menuIconXpath         = "//div[@class='nav-icon']";
	private static final String menuXpath             = "//div[@class='header-menu']";
	private static final String footerXpath           = "//span[@id='trivago-magazine-footer']";
	private static final String contactXpath          = "//div/a[contains(text(), 'Contact')]";
	private static final String emailAddrXpath        = "//input[@class='et-email']";
	private static final String inspireMeXpath        = "//div[@class='newsletter-submit']";
	private static final String checkedInXpath        = "//div/p[contains(text(), 'You are now checked-in!')]";
	private static final String firstMenuDestXpath    = "//div[contains(text(), 'West')]";
	private static final String heroButtonXpath       = "//div[@class='hero-button']";

	
	public static String getSearchIconXpath() {
		return searchIconXpath;
	}
		
	public static String getMenuIconXpath() {
		return menuIconXpath;
	}
	
	public static String getMenuXpath() {
		return menuXpath;
	}

	public static String getFooterXpath() {
		return footerXpath;
	}
	
	public static String getContactXpath() {
		return contactXpath;
	}
	
	public static String getEmailAddrXpath() {
		return emailAddrXpath;
	}
	
	public static String getInspireMeXpath() {
		return inspireMeXpath;
	}
	
	public static String getCheckedInXpath() {
		return checkedInXpath;
	}
	
	public static String getFirstMenuDestXpath() {
		return firstMenuDestXpath;
	}
	
	public static String getHeroButtonXpath() {
		return heroButtonXpath;
	}
}

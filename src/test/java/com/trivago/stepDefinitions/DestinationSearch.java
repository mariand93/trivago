package com.trivago.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.trivago.base.BrowserInstance;
import com.trivago.elements.ArticleElements;
import com.trivago.elements.HomeElemets;
import com.trivago.elements.SearchElements;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DestinationSearch{

	BrowserInstance browser = new BrowserInstance();
	WebDriver driver = browser.start();
	 
	@Given("^user opens trivago page52$")
	public void user_opens_trivago_page52(){
		driver.get(browser.getTestUrl());
		
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getSearchIconXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getMenuIconXpath())).isDisplayed());
	}
	
	
	@When("^user clicks on search top right icon$")
	public void user_clicks_on_search_top_right_icon(){
		driver.findElement(By.xpath(HomeElemets.getSearchIconXpath())).click();
		
		Assert.assertTrue(driver.findElement(By.xpath(SearchElements.getSearchInputXpath())).isDisplayed());
	}
	
	@And("^user enters \"(.*)\" in search text area$")
	public void user_enters_city_in_search_text_area(String city){
		driver.findElement(By.xpath(SearchElements.getSearchInputXpath())).sendKeys(city); 
		driver.findElement(By.xpath(SearchElements.getSearchInputXpath())).sendKeys(Keys.RETURN);
	}
	
	@Then("^user should see destinations results$")
	public void user_should_see_destinations_results() {
		Assert.assertTrue(driver.findElement(By.xpath(SearchElements.getSearchResultsXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(SearchElements.getPostSectionXpath())).isDisplayed());
	}
	 
	@And("^user clicks on first article$")
	public void user_clicks_on_first_article(){
		driver.findElement(By.xpath(SearchElements.getPostCardXpath())).click();
	}
	 
	@Then("^user should see article opened$")
	public void user_should_see_article_opened() {
		Assert.assertTrue(driver.findElement(By.xpath(ArticleElements.getArticleContentXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(ArticleElements.getDestinationTagXpath())).isDisplayed());
		
		driver.quit();
	}
}
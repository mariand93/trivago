package com.trivago.stepDefinitions;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.trivago.base.BrowserInstance;
import com.trivago.elements.ArticleElements;
import com.trivago.elements.ContactElements;
import com.trivago.elements.HomeElemets;
import com.trivago.elements.SearchElements;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ContactForm {

	BrowserInstance browser = new BrowserInstance();
	WebDriver driver = browser.start();
	 
	@Given("^user opens trivago page2$")
	public void user_opens_trivago_page2(){
		driver.get(browser.getTestUrl());
		
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getSearchIconXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getMenuIconXpath())).isDisplayed());
	}
	
	
	@When("^user clicks on footer contact link$")
	public void user_clicks_on_footer_contact_link(){
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		driver.findElement(By.xpath(HomeElemets.getContactXpath())).click();
	}
	
	@When("^new tab is opened user should see contact form$")
	public void new_tab_is_opened_user_should_see_contact_form() {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		
		Assert.assertTrue(driver.findElement(By.xpath(ContactElements.getEmailInputXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(ContactElements.getHeaderTextXpath())).isDisplayed());
	}
		
	@And("^user enters \"(.*)\" in Your Message input$")
	public void user_enters_message_in_your_message_input(String message){
		driver.findElement(By.xpath(ContactElements.getContactMsgXpath())).sendKeys(message); 
	}
	
	@And("^user enters \"(.*)\" and \"(.*)\"$")
	public void user_enters_name_and_email(String name, String email){
		driver.findElement(By.xpath(ContactElements.getNameInputXpath())).sendKeys(name); 
		driver.findElement(By.xpath(ContactElements.getEmailInputXpath())).sendKeys(email);
	}
	
	@And("^user clicks on submit button$")
	public void user_clicks_on_submit_button(){
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		driver.findElement(By.xpath(ContactElements.getConfirmXpath())).click();
		driver.findElement(By.xpath(ContactElements.getSubmitXpath())).click();
	}
	 
	@Then("^user should see successfully sent message$")
	public void user_should_see_successfully_sent_message() {
		Assert.assertTrue(driver.findElement(By.xpath(ContactElements.getSuccessMsgXpath())).isDisplayed());
		
		driver.quit();
	}
}
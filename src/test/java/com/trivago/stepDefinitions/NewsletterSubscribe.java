package com.trivago.stepDefinitions;

import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.trivago.base.BrowserInstance;
import com.trivago.elements.ArticleElements;
import com.trivago.elements.ContactElements;
import com.trivago.elements.HomeElemets;
import com.trivago.elements.SearchElements;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NewsletterSubscribe {

	BrowserInstance browser = new BrowserInstance();
	WebDriver driver = browser.start();
	 
	@Given("^user opens trivago page3$")
	public void user_opens_trivago_page3(){
		driver.get(browser.getTestUrl());
		
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getSearchIconXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getMenuIconXpath())).isDisplayed());
	}
	
	@When("^user enters email in newsletter input$")
	public void user_enters_email_in_newsletter_input(){
		driver.findElement(By.xpath(HomeElemets.getEmailAddrXpath())).sendKeys(ThreadLocalRandom.current().nextInt(100, 100000) + "@email.ro"); 
	}
	
	@And("^user clicks on inspire me button$")
	public void user_clicks_on_inspire_me_button(){
		driver.findElement(By.xpath(HomeElemets.getInspireMeXpath())).click();
	}
	
	@Then("^user should see you are now checked in message$")
	public void user_should_see_successfully_sent_message() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getCheckedInXpath())).isDisplayed());
		
		driver.quit();
	}
}
package com.trivago.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.trivago.base.BrowserInstance;
import com.trivago.elements.ArticleElements;
import com.trivago.elements.HomeElemets;
import com.trivago.elements.SearchElements;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ChooseDestination{

	BrowserInstance browser = new BrowserInstance();
	WebDriver driver = browser.start();
	 
	@Given("^user opens trivago page4$")
	public void user_opens_trivago_page4(){
		driver.get(browser.getTestUrl());
		
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getSearchIconXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getMenuIconXpath())).isDisplayed());
	}
		
	@When("^user clicks on hamburger menu icon$")
	public void user_clicks_on_hamburger_menu_icon(){
		driver.findElement(By.xpath(HomeElemets.getMenuIconXpath())).click();
		
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getFirstMenuDestXpath())).isDisplayed());
	}
	
	@And("^user selects first menu destination$")
	public void user_selects_first_menu_destination(){
		driver.findElement(By.xpath(HomeElemets.getFirstMenuDestXpath())).click();
	}

	@Then("^user should see destination opened$")
	public void user_should_see_destination_opened() {
		Assert.assertTrue(driver.findElement(By.xpath(SearchElements.getDestSectionXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(SearchElements.getFilterXpath())).isDisplayed());
		
		driver.quit();
	}
}
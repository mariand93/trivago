package com.trivago.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.trivago.base.BrowserInstance;
import com.trivago.elements.ArticleElements;
import com.trivago.elements.HomeElemets;
import com.trivago.elements.SearchElements;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CommentArticle{

	BrowserInstance browser = new BrowserInstance();
	WebDriver driver = browser.start();
	 
	@Given("^user opens trivago page51$")
	public void user_opens_trivago_page51(){
		driver.get(browser.getTestUrl());
		
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getSearchIconXpath())).isDisplayed());
		Assert.assertTrue(driver.findElement(By.xpath(HomeElemets.getMenuIconXpath())).isDisplayed());
	}
		
	@When("^user clicks on read more$")
	public void user_clicks_on_read_more(){
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 200)");
		driver.findElement(By.xpath(HomeElemets.getHeroButtonXpath())).click();
	}
	
	@And("^user clicks on comments$")
	public void user_clicks_on_comments(){
		WebElement el = driver.findElement(By.xpath(ArticleElements.getShowCommentsXpath()));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", el);
		el.click();
	}
	
	@And("^user clicks on sign is with email$")
	public void user_clicks_on_sign_in_with_email(){
		driver.findElement(By.xpath(ArticleElements.getSignInEmailXpath())).click();
	}
	
	@And("^user enters \"(.*)\" and \"(.*)\" in login form$")
	public void user_enter_name_and_email_in_login_form(String name, String email){
		driver.findElement(By.xpath(ArticleElements.getLoginNameXpath())).sendKeys(name); 
		driver.findElement(By.xpath(ArticleElements.getLoginEmailXpath())).sendKeys(email); 
		driver.findElement(By.xpath(ArticleElements.getLoginButtonXpath())).click();
	}
	
	@Then("^user enters \"(.*)\" comment$")
	public void user_enters_comm_comment(String comm) {
		driver.findElement(By.xpath(ArticleElements.getCommentInputXpath())).sendKeys(comm);
		driver.findElement(By.xpath(ArticleElements.getSentCommentXpath())).click();
	}

	@Then("^user should see that comment is sent and waits for approval$")
	public void user_should_see_that_comment_is_sent_and_waits_for_approval() {
		Assert.assertTrue(driver.findElement(By.xpath(ArticleElements.getCommentSuccessXpath())).isDisplayed());
		
		driver.quit();
	}
}
Feature: Contact Form

Scenario Outline: Fill in the contact form and send it

Given user opens trivago page2
When user clicks on footer contact link
When new tab is opened user should see contact form
And user enters "<message>" in Your Message input
And user enters "<name>" and "<email>" 
And user clicks on submit button
Then user should see successfully sent message

Examples:
	|                  message                              |  name   |    email    |
	|Hello. This is an Automation Test message. Kind Regards|Test Name|test@email.ro|
		
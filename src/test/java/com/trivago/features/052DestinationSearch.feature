Feature: Destination Search

Scenario Outline: Search destination and open article

Given user opens trivago page52
When user clicks on search top right icon
And user enters "<city>" in search text area
Then user should see destinations results
And user clicks on first article
Then user should see article opened

Examples:
	| city  |
	| Miami |
		
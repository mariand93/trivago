package com.trivago.base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowserInstance {
	private String driverPath = "C:\\testgit\\trivago\\chromedriver.exe";
	private String testUrl     = "http://magazine.trivago.com/";
	
	private WebDriver driver;
	private WebDriverWait waitDriver;
	
	public WebDriver start(){
	 System.setProperty("webdriver.chrome.driver", driverPath);
	 driver = new ChromeDriver();
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 driver.manage().window().maximize();
	 
	 System.out.println("aici");
	 return driver;
	}
	
	public String getTestUrl(){
		return testUrl;
	}
	
}
